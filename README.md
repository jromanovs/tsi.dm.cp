Turing machine implementation on Rust. Bascic CLI interface. Coursework for Discrete mathematics.

# Usage examples

To build and open documentation:

	cargo doc --open

To run a sample machine:

	cargo run fixtures/case01.turing

or

	cargo run fixtures/case01.turing aaaaabbbbb

To build standalone application:

	cargo build

To run tests:

	cargo test

# Command line parameters

	turing <filename> [input tape] [--verbose]

where

* **filename** configuration file for machine contain all program and optional tape and expected resulting tape
* **input tape** starting tape, optional argument can be specified in the **filename** or entered from a console prompt
* **--verbose** show detailed Turing Machine state during execution, optional argument

