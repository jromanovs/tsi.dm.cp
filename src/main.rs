//! The Turing Machine command line simulator
//!
//! # Usage
//!
//!     turing <filename> [input tape] [--verbose]
//!
//! where
//! * **filename** configuration file for machine contain all program and optional tape and expected resulting tape
//! * **input tape** starting tape, optional argument can be specified in the **filename** or entered from a console prompt
//! * **--verbose** show detailed Turing Machine state during execution, optional argument
//!
//! The configuration file must contain lines with either comments started with # letter, or commands for a Turing Machine.
//! Commands must be specified in following format:
//!
//!     <state>,[letter],<new state>,[print letter],<head command>
//!
//! where
//! * **state** the current state for the machine
//! * **letter** the current symbol read by head, empty for blank
//! * **new state** next state to change
//! * **print letter** next symbol to print, empty for blank
//! * **head command** move command for head: **L** left, **R** right, or **N** none
//!
//! Anything after # is ignores with exception of the two "special" comments:
//!
//!     ## Text file sample contained list of commands for the Turing Machine,
//!     ## the input sample, and the expected result. See below:
//!     ## sample: 010101110101
//!     ## expect: 111111111111
//!
//!     # Fill with 1's.
//!     S0,0,S0,1,R
//!     S0,1,S0,1,R
//!
//!     # Go to the reverse direction
//!     S0,,S1,,L
//!
//!     # Go to the beginning of the line
//!     S1,1,S1,1,L
//!
//!     # Stop machine, as a stop state can be used any letter sequence
//!     S1,,Halt,R
//!
//! A line with *sample* keyword specifies initial tape. A line with *expect* keyword specifies
//! expected tape after machine stops.
//!
#![warn(missing_docs)]
#![warn(rustdoc::missing_doc_code_examples)]

use std::{env, io};
use std::collections::HashMap;
use std::fmt::Formatter;
use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;
use std::process::exit;
use std::path::Path;

#[derive(Debug)]
enum HeadCommand {
    Stay,
    MoveLeft,
    MoveRight,
}

impl std::fmt::Display for HeadCommand {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            HeadCommand::Stay => "N",
            HeadCommand::MoveLeft => "L",
            HeadCommand::MoveRight => "R",
        })
    }
}

/// Tuple for the Turing Machine state
#[derive(Debug, Eq, Hash, PartialEq)]
pub struct FullStatus {
    state_index: usize,
    letter_index: Option<usize>,
}

/// Triple for the Turing Machine command
#[derive(Debug)]
pub struct Command {
    new_state: usize,
    new_letter: Option<usize>,
    head_command: HeadCommand,
}

/// Turing Machine components
#[derive(Debug)]
pub struct TuringMachine {
    /// vector of alphabet strings for example: "0", "1"
    alphabet: Vec<String>,
    /// vector of allowed states for example: "S0", "S1", "S2"
    states: Vec<String>,
    /// vector of indexes to the alphabet for current tape information,
    /// by None marked blank places
    tape: Vec<Option<usize>>,
    /// the head position on the tape
    head: usize,
    /// program for the machine, the first tuple is for the current state,
    /// the second triple stands for assigned state, and command
    program: HashMap<FullStatus, Command>,
    /// the current state of the machine
    current: usize,
}

impl TuringMachine {
    /// Creates a new empty `TuringMachine`.
    ///
    /// # Examples
    ///
    /// Basic usage:
    ///
    /// ```rust
    /// let tm = TuringMachine::new();
    /// tm.load_from_file("sample.turing");
    /// tm.run();
    /// ```
    pub fn new() -> Self {
        TuringMachine {
            alphabet: vec![],
            states: vec![],
            tape: vec![],
            head: 0,
            program: Default::default(),
            current: 0
        }
    }

    fn find_or_add_state(&mut self, state: &str) -> usize {
        let ix = self.states.iter().position(|x| x.as_str() == state);
        match ix {
            Some(value) => value,
            None => {
                self.states.push(state.to_string());
                self.states.len()-1
            }
        }
    }

    fn find_or_add_letter(&mut self, letter: &str) -> Option<usize> {
        if letter.len() == 0 {
            None
        } else {
            let ix = self.alphabet.iter().position(|x| x.as_str() == letter);
            match ix {
                Some(value) => Some(value),
                None => {
                    self.alphabet.push(letter.to_string());
                    Some(self.alphabet.len() - 1)
                }
            }
        }
    }

    fn new_command(&mut self, command: &str) {
        let split = command.split(",").collect::<Vec<&str>>();
        if split.len() < 5 { panic!("command must have at least 5 elements {}",command) }
        let prev_status = self.find_or_add_state(split[0]);
        let prev_letter = self.find_or_add_letter(split[1]);
        let new_status = self.find_or_add_state(split[2]);
        let new_letter = self.find_or_add_letter(split[3]);
        let head_command  = match split[4] {
            "R" => HeadCommand::MoveRight,
            "L" => HeadCommand::MoveLeft,
            _ => HeadCommand::Stay,
        };
        self.program.insert(
            FullStatus{ state_index: prev_status, letter_index: prev_letter },
            Command{ new_state: new_status, new_letter, head_command }
        );
    }

    fn next_step(&mut self) -> bool {
        let current_letter = self.tape[self.head];
        let command = self.program.get(
            &FullStatus{ state_index: self.current, letter_index: current_letter });
        match command {
            Some(command) => {
                self.tape[self.head] = command.new_letter;
                self.current = command.new_state;
                match command.head_command {
                    HeadCommand::MoveRight => {
                        if self.head == 0 && self.tape[0].is_none() {
                            self.tape.remove(0);
                        } else {
                            self.head += 1;
                            if self.tape.len() == self.head {
                                self.tape.push(None);
                            }
                        }
                        true
                    },
                    HeadCommand::MoveLeft => {
                        if self.head == self.tape.len() - 1 && self.tape[self.head].is_none() {
                            self.tape.remove(self.head);
                            self.head -= 1;
                        } else {
                            if self.head == 0 {
                                self.tape.insert(0, None);
                            } else {
                                self.head -= 1;
                            }
                        }
                        true
                    }
                    _ => {
                        println!("Stay command reached");
                        false
                    },
                }
            },
            None => {
                println!("No more commands available");
                false
            }
        }
    }

    fn load_from_file(&mut self, filename: &str) -> Option<String> {
        let f = match File::open(filename) {
            Ok(file) => file,
            Err(e) => panic!("Error open Turing Machine file {}",e.to_string()),
        };
        let f = BufReader::new(f);
        let mut result = None;

        for line in f.lines() {
            let l = line.unwrap();
            if l.len() == 0 || l.starts_with("#") {
              if l.contains("sample:") {
                  let tape = l.split("sample:").nth(1);
                  if tape.is_some() {
                      self.load_tape(tape.unwrap().trim());
                  }
              } else if l.contains("expect:") {
                  let tape = l.split("expect:").nth(1);
                  if tape.is_some() {
                      result = Some(tape.unwrap().trim().to_string());
                  }
              }
            } else {
                self.new_command(&l);
            }
        }
        result
    }

    fn load_tape(&mut self, tape: &str) {
        self.tape.clear();
        for c in tape.chars() {
            let letter = self.find_or_add_letter(&c.to_string());
            self.tape.push(letter);
        }
    }

    fn print_step(&self, index: i32) {
        println!("step {}: [{}-{:0>4}] {}",
               index,
               self.states[self.current],
               self.head,
               self.get_tape()
        );
    }

    fn run(&mut self) -> String {
        while self.next_step() { }
        self.get_tape()
    }

    fn run_verbose(&mut self) -> String {
        let mut i = 0;
        while self.next_step() {
            i += 1;
            self.print_step(i)
        }
        self.get_tape()
    }

    fn get_tape(&self) -> String {
        let letters : Vec<&str> = self.tape.iter().map(|&x|
            match x {
                Some(v) => { &self.alphabet[v] },
                None => { " " },
            }
        ).collect();
        letters.join("").trim().to_string()
    }

    fn get_program(&self) -> String {
        let mut p= Vec::new();
        for (key, value) in &self.program {
            let letter: &str = match key.letter_index {
                Some(ix) => &self.alphabet[ix],
                None => "",
            };
            let new_letter: &str = match value.new_letter {
                Some(ix) => &self.alphabet[ix],
                None => "",
            };
            p.push(format!(
                "{},{},{},{},{}",
                &self.states[key.state_index],
                letter,
                &self.states[value.new_state],
                new_letter,
                value.head_command,
            ));
        }
        p.join("\n")
    }
}

impl std::fmt::Display for TuringMachine {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.get_program())
    }
}

fn print_usage() {
    println!("\
    Turing Machine CLI\n\
    Copyright (c) 2021 Jurijs Romanovs MIT License\n\
    Usage:\n\
    turing <file> [tape] [--verbose]\n");
}

fn main() {
    let mut args: Vec<String> = env::args().collect();
    let mut verbose = false;

    if let Some(ix) = args.iter().position(|x| *x == "--verbose") {
        args.remove(ix);
        verbose = true;
    }

    if args.len() < 2 {
        print_usage();
        exit(0);
    }

    let filename = &args[1];
    if !Path::new(filename).exists() {
        println!("File with commands '{}' is expected, but does not exists", filename);
        exit(-1);
    }

    let mut tm = TuringMachine::new();
    let mut expect = tm.load_from_file(filename);
    if args.len() > 2 {
        tm.load_tape(&args[2]);
        expect = None;
    } else {
        if tm.get_tape().is_empty() {
            println!("Tape was not supplied in the file, please enter a tape:");
            let mut tape = String::new();
            io::stdin()
                .read_line(&mut tape)
                .expect("Failed to read tape");
            tm.load_tape(&tape);
        }
    }

    let result = if verbose {
        println!("Turing machine program:\n{}", tm);
        tm.run_verbose()
    } else { tm.run() };

    if let Some(ex) = expect {
        if result == ex {
            println!("TEST PASSED: {}", result);
        } else {
            println!("TEST FAILED: {}, expected {}", result, ex);
        }
    } else {
        println!("{}", result);
    }
}

#[cfg(test)]
mod tests {
    use std::fs;
    use super::*;

    #[test]
    fn basic_functionality_test() {
        let mut tm = TuringMachine::new();
        tm.load_tape("01");
        tm.new_command("S0,0,S0,1,R");
        tm.new_command("S0,1,S0,0,R");
        assert_eq!(tm.run(), "10");
    }

    #[test]
    fn case01() {
        let mut tm = TuringMachine::new();
        let expect = tm.load_from_file("fixtures/case01.turing");
        assert_eq!(Some(tm.run()), expect);
    }

    #[test]
    fn case02() {
        let mut tm = TuringMachine::new();
        let expect = tm.load_from_file("fixtures/case02.turing");
        assert_eq!(Some(tm.run()), expect);
    }

    #[test]
    fn case07() {
        let mut tm = TuringMachine::new();
        let expect = tm.load_from_file("fixtures/case07.turing");
        assert_eq!(Some(tm.run()), expect);
    }

    #[test]
    fn case13() {
        let mut tm = TuringMachine::new();
        let expect = tm.load_from_file("fixtures/case13.turing");
        assert_eq!("11111/11", tm.get_tape());
        assert_eq!(Some(tm.run()), expect);
    }

    #[test]
    fn todo_all_tests() {
        let paths = fs::read_dir("fixtures/").unwrap();
        for entry in paths {
            let entry = entry.unwrap();
            let path = entry.path();
            assert_eq!("fixtures/case12.turing", path.to_string_lossy());
            break;
        }
    }
}
